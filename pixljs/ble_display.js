const NC = require("nodeconfeu2018"); // http://www.espruino.com/modules/nodeconfeu2018.js

const HEX = 0x10;
const SERVICE_DATA_128BIT_UUID = 0x21;

//const customService = '91702018-BAF8-49A8-BF48-CB9B83120203'.toLowerCase().replace('-', '');
const customService = '91702018baf849a8bf48cb9b83120203';

function ramp() {
	var n = 0;
	setTimeout(cb() => {
		analogWrite(VIBL, Math.sin(n));
		n+=0.01;
		if (n < Math.PI) setTimeout(cb, 20);
		else digitalWrite(VIBL, 0);
	}, 20);
}

function scanResult(device) {
	const {serviceData } = device;
	if (serviceData && serviceData[customService]) {
		parseServiceData(serviceData[customService]);
	}
}

var lastCard = {fc: 0, cn: 0};

function parseServiceData(data) {
	const stringData = String.fromCharCode(data);
	// TODO: check for empty and return early

	if (stringData === 'ready') {
		g.clear();
		g.setFontVector(30);
		g.drawString("READY");
		battDisplay();
		g.flip();
		return;
	} else if (stringData === 'offli') {
		g.clear();
		g.setFontVector(30);
		g.drawString("OFFLINE");
		battDisplay();
		g.flip();
		return;
	}

	const card = {
		fc: data[0],
		cn: (data[1] << 8) | data[2],
	};

	if (lastCard.fc !== card.fc || lastCard.cn !== card.cn) {
		if (lastCard.fc !== card.fc) {
			digitalPulse(VIBR, 1, 100);

		}
		if (lastCard.cn !== card.cn) {
			digitalPulse(VIBL, 1, 100);
		}
		lastCard = card;

		g.clear();
		g.setFontVector(20);

		const fcString = "FC: " + card.fc;
		const cnString = "CN: " + card.cn;
		const cardDisplay = [fcString, cnString].join('\n');
		const x = g.getWidth() - g.stringWidth(cardDisplay);
		const y = g.getHeight() - g.stringHeight(cardDisplay);
		g.drawString(cardDisplay, x/2, y/2);

		battDisplay();
		g.flip();
	}
}


function battDisplay() {
	const battery = E.getBattery();
	digitalWrite(LED1, battery < 50);
	digitalWrite(LED2, battery < 25);

	try {
		// Show battery bar chart when not on power
		const { charging, standby } = NC.getBatteryState();
		if (!charging && !standby) {
			const battChart = battery / 100 * g.getWidth();
			g.fillRect(0, g.getHeight() - 4, battChart, g.getHeight());
		}
	} catch (e) {}
}


function setBacklight(b) {
	// Along the bottom, facing up,
	// starting on left, going to the right
	// B,G,R

	NC.backlight([
		b, b, b,
		b, b, b,
		b, b, b,
		b, b, b,
	]);
}


function parse128BitServiceData(device) {
	const { data, serviceData = {} } = device;
	if (data) {
		dataArray = new Uint8Array(data);
		var index = 0;
		do {
			const length = data[index++];
			const field_type = data[index];
			if (field_type === SERVICE_DATA_128BIT_UUID) {
				const field = dataArray.slice(index + 1, index + 1 + length);
				const uuid = field.slice(0, 16);
				const uuidArray = [].map.call(uuid, x=>x.toString(HEX));
				uuidArray.reverse();
				const uuidHex = uuidArray.map(s => s.length === 1 ? '0' + s : s).join('');
				const value = field.slice(uuid.length);
				serviceData[uuidHex] = new Uint8Array(value).buffer;
				device.serviceData = serviceData;
			}
			index += length;
		} while (index < data.length);
	}
	return device;
}

const scanTimeout = 500;

NC.ledTop();
NC.ledBottom();
setBacklight(170);

function scan() {
	NRF.findDevices((devices) => {
		devices.map(parse128BitServiceData).forEach(scanResult);
	}, scanTimeout);
}

const scanning = setInterval(scan, 2 * scanTimeout);

// Controls to start/stop search, scroll through previous cards
setWatch(scan, BTN1, { edge:"rising", debounce:50, repeat: true });
setWatch(scan, BTN2, { edge:"rising", debounce:50, repeat: true });
setWatch(scan, BTN3, { edge:"rising", debounce:50, repeat: true });
setWatch(scan, BTN4, { edge:"rising", debounce:50, repeat: true });

