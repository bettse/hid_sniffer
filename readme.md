# HID Sniffer

(I suck at naming).  This is my take on the RFID Tastic-Thief using BLE to advertise the FC and CN to be collected by another device.

This is the code for an [Adafruit Feature 32u4 Bluefruit LE](https://www.adafruit.com/product/2829) connected to a Maxi Prox 5375AGN00 using a [5v to 3.3v logic level converter](https://www.amazon.com/gp/product/B07H2C6SJJ/ref=ppx_od_dt_b_asin_title_s00?ie=UTF8&psc=1).

If I were doing this again, I'd probably use the [Adafruit Feature nRF42 Bluefruit LE](https://www.adafruit.com/product/3406), but I didn't a spare one on hand at the time.

The Pixljs folder contains a client for the [Espruino Pixljs](https://shop.espruino.com/pixljs), and I have an iOS client written in Swift in [another repo](https://gitlab.com/bettse/hidsniffclient).  The iOS client watches for the advertisements, captures the data, and tags it with time and location, allowing later correlation.
