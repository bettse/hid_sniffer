from odo.models import StateModel
from odo.credentials import WiegandCredential, WiegandPayload

class BleScannerStatePayload(object):
    def __init__(self, status="disconnected", *args, **kwargs):
        self.status = status

class BleScannerStateModel(StateModel):
    def __init__(self, payload=dict(), *args, **kwargs):
        super(BleScannerStateModel, self).__init__(*args, **kwargs)
        self.payload = BleScannerStatePayload(**payload)

class BleScannerPayload(WiegandPayload):
    def __init__(self, bits=None, hex=None, timestamp=None, *args, **kwargs):
        self.bits = bits
        self.hex = hex
        self.timestamp = timestamp

class BleScannerCredential(WiegandCredential):
    def __init__(self, payload=dict(), *args, **kwargs):
        super(BleScannerCredential, self).__init__(*args, **kwargs)
        self.payload = BleScannerPayload(**payload)

