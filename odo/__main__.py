import logging
from ble_scanner import BleScanner

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - [%(name)s] - %(levelname)s - %(message)s [%(threadName)s]',
)

if __name__ == "__main__":
    scanner = BleScanner()
    scanner.start()
    try:
        input("Press enter key to end\n")
    except KeyboardInterrupt:
        pass
    scanner.terminate()
    scanner.join()
