import asyncio
import logging
import json
import binascii
import time
from bleak import BleakScanner
from bleak import _logger as logger
from odo.models import BaseMqttDeviceModel
from .models import BleScannerStateModel, BleScannerCredential

logging.getLogger("bleak.backends.bluezdbus.scanner").setLevel(logging.WARNING)
logging.getLogger("bleak.backends.corebluetooth.CentralManagerDelegate").setLevel(logging.WARNING)

service_uuid = "0000181c-0000-1000-8000-00805f9b34fb" # Long form of 0x181c

class BleScanner(BaseMqttDeviceModel):
    def __init__(self, *args, **kwargs):
        super(BleScanner, self).__init__(*args, **kwargs)
        self.logger = logging.getLogger('odo.ble.BleScanner')
        self.event_loop = asyncio.new_event_loop()
        self.queue = asyncio.Queue()
        self.client = None
        self._event = None
        self.state = BleScannerStateModel()
        self._subscribe_topics = [
            self.command_topic
        ]
        self.latest_credential = None

    """Overloads _send_state() to not call self.mqtt_client.loop()"""
    def _send_state(self):
        self.mqtt_client.publish(self.state_topic, self.state.to_json())

    def _handle_command(self, msg=None):
        self.logger.error(f"Command parsing not implemented")

    def _on_message(self, client, userdata, msg):
        self.logger.debug(f"MQTT message received-> {msg.topic} {str(msg.payload)}")
        if msg.topic == self.command_topic:
            self._handle_command(msg=msg)

    async def mqtt_loop(self):
        while True:
            if self.state.payload.status == "connected":
                self.mqtt_client.loop()
                await asyncio.sleep(0.5)
            else:
                await asyncio.sleep(10)

    #AdvertisementData(local_name='iWatch', service_data={'91702018-baf8-49a8-bf48-cb9b83120203': b'\xf8C4\x00\x00'})
    def detection_callback(self, device, advertisement_data):
        if service_uuid in advertisement_data.service_data:
            value = advertisement_data.service_data[service_uuid]
            bits = value[0]
            wlen = int((bits + 7) / 8)
            wiegand = value[1:wlen+1]
            if bits == 0:
                #wiegand is a status string
                self.logger.debug(wiegand)
                return
            h = binascii.hexlify(wiegand).decode()
            if h == self.latest_credential:
                return
            self.latest_credential = h
            credential = BleScannerCredential(payload={
                "bits": bits,
                "hex": h,
                "timestamp": int(time.time()),
            })
            self.logger.debug(f"Publish -> {credential.to_json()}")
            self.mqtt_client.publish(self.credential_topic["seen"], credential.to_json())

    async def scanner(self):
        while True:
            try:
                scanner = BleakScanner(timeout=1.0)
                scanner.register_detection_callback(self.detection_callback)
                await scanner.start()
                await asyncio.sleep(1.0)
            except ValueError as e:
                self.logger.error(e)
            except bleak.exc.BleakError as e:
                self.logger.error(e)

    async def manage(self):
        tasks = []
        tasks.append(self.event_loop.create_task(self.mqtt_loop()))
        tasks.append(self.event_loop.create_task(self.scanner()))
        await asyncio.gather(*tasks)

        self._event = asyncio.Event()
        await self._event.wait()

    def loop(self):
        self._send_state()
        asyncio.set_event_loop(self.event_loop)
        self.event_loop.run_until_complete(self.manage())
        self.event_loop.close()

    def _cleanup(self):
        self._disconnect()
        self._event.set()

