#include "Adafruit_BLE.h"
#include "Adafruit_BluefruitLE_SPI.h"
#include <SPI.h>
#include <Wiegand.h>

#if SOFTWARE_SERIAL_AVAILABLE
#include <SoftwareSerial.h>
#endif

// These are the pins connected to the Wiegand D0 and D1 signals.
// Ensure your board supports external Interruptions on these pins
#define PIN_D0 0
#define PIN_D1 1

#define LED_BUILTIN 13
#define HAPTIC_PIN 23

#define BLUEFRUIT_SPI_CS 8
#define BLUEFRUIT_SPI_IRQ 7
#define BLUEFRUIT_SPI_RST 4 // Optional but recommended, set to -1 if unused
#define VERBOSE_MODE false // If set to 'true' enables debug output

#define FACTORYRESET_ENABLE 0
#define MINIMUM_FIRMWARE_VERSION "0.7.0"
#define MODE_LED_BEHAVIOUR "DISABLE"

#define SERVICE_DATA_16BIT_UUID 0x16
#define SERVICE_DATA_128BIT_UUID 0x21
#define CUSTOM_SERVICE_UUID "91702018-BAF8-49A8-BF48-CB9B83120203"
#define SERVICE_DATA_LEN 10

Adafruit_BluefruitLE_SPI ble(BLUEFRUIT_SPI_CS, BLUEFRUIT_SPI_IRQ, BLUEFRUIT_SPI_RST);

Wiegand wiegand;

// When any of the pins have changed, update the state of the wiegand library
void pinStateChanged() {
  wiegand.setPin0State(digitalRead(PIN_D0));
  wiegand.setPin1State(digitalRead(PIN_D1));
}

// Notifies when a reader has been connected or disconnected.
// Instead of a message, the seconds parameter can be anything you want -- Whatever you specify on `wiegand.onStateChange()`
void stateChanged(bool plugged, const char *message) {
  Serial.print(message);
  Serial.println(plugged ? "CONNECTED" : "DISCONNECTED");
  if (plugged) {
    setServiceData(String("CONNECTED").c_str());
    blink(2, 1000);
  } else {
    setServiceData(String("DISCONNECTED").c_str());
    blink(1, 1000);
  }
}

// Notifies when a card was read.
// Instead of a message, the third parameter can be anything you want -- Whatever you specify on `wiegand.onReceive()`
void receivedData(uint8_t *data, uint8_t bits, const char *message) {
  Serial.print(message);
  Serial.print(bits);
  Serial.print("bits / ");
  //Print value in HEX
  uint8_t bytes = (bits + 7) / 8;
  Serial.print("0x");
  for (int i = 0; i < bytes; i++) {
    if (data[i] < 0x10) {
      Serial.print(0);
    }
    Serial.print(data[i], HEX);
  }
  Serial.println(" |");

  setServiceData(data, bits);
  blink(1, 2000);
}

// Notifies when an invalid transmission is detected
void receivedDataError(Wiegand::DataError error, uint8_t *rawData, uint8_t rawBits, const char *message) {
  Serial.print(message);
  Serial.print(Wiegand::DataErrorStr(error));
  Serial.print(" - Raw data: ");
  Serial.print(rawBits);
  Serial.print("bits / ");

  //Print value in HEX
  uint8_t bytes = (rawBits + 7) / 8;
  for (int i = 0; i < bytes; i++) {
    Serial.print(rawData[i] >> 4, 16);
    Serial.print(rawData[i] & 0xF, 16);
  }
  Serial.println();
}

void error(const __FlashStringHelper *err) {
  Serial.println(err);
  //while (1);
}

void setServiceData (const char *data) {
  setServiceData((uint8_t *)data);
}

void setServiceData (uint8_t *data) {
  setServiceData(data, 0);
}

void setServiceData (uint8_t *data, uint8_t bits) {
  uint8_t advdata[]{
    0x02, 0x01, 0x06,
    14/*len*/, SERVICE_DATA_16BIT_UUID, 0x1c, 0x18, bits, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff
  };
  if (bits == 0) { //string
    uint8_t bytes = strnlen((char*)data, SERVICE_DATA_LEN);
    memcpy(advdata + 8, data, bytes);
  } else {
    uint8_t bytes = (bits + 7) / 8;
    memcpy(advdata + 8, data, bytes);
  }
  ble.setAdvData(advdata, sizeof(advdata));
}

// Initialize Wiegand reader
void setup() {
  pinMode(PIN_D0, INPUT);
  pinMode(PIN_D1, INPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);

  Serial.begin(115200);
  // while (!Serial) delay(10);

  if (!ble.begin(VERBOSE_MODE)) {
    error(F("Couldn't find Bluefruit, make sure it's in CoMmanD mode & check wiring?"));
  }
  Serial.println(F("OK!"));

  if (FACTORYRESET_ENABLE) {
    /* Perform a factory reset to make sure everything is in a known state */
    Serial.println(F("Performing a factory reset: "));
    if (!ble.factoryReset()) {
      error(F("Couldn't factory reset"));
    }
  }

  /* Disable command echo from Bluefruit */
  ble.echo(false);

  Serial.println("Requesting Bluefruit info:");
  /* Print Bluefruit information */
  ble.info();

  if (!ble.sendCommandCheckOK(F("AT+GAPDEVNAME=iWatch"))) {
    error(F("Could not set device name?"));
  }

  uint8_t empty[SERVICE_DATA_LEN] = {0};
  setServiceData(empty);
  /* Reset the device for the new service setting changes to take effect */
  // Serial.print(F("Performing a SW reset (service changes require a reset): "));
  // ble.reset();
  // Serial.println();

  //Install listeners and initialize Wiegand reader
  wiegand.onReceive(receivedData, "Card read: ");
  wiegand.onReceiveError(receivedDataError, "Card read error: ");
  wiegand.onStateChange(stateChanged, "State changed: ");
  wiegand.begin(Wiegand::LENGTH_ANY, false);

  //initialize pins as INPUT and attaches interruptions
  attachInterrupt(digitalPinToInterrupt(PIN_D0), pinStateChanged, CHANGE);
  attachInterrupt(digitalPinToInterrupt(PIN_D1), pinStateChanged, CHANGE);

  pinStateChanged(); // Will open file for writing

  blink(3, 300);
}

// Every few milliseconds, check for pending messages on the wiegand reader
// This executes with interruptions disabled, since the Wiegand library is not thread-safe
void loop() {
  noInterrupts();
  wiegand.flush();
  interrupts();
}

void blink(uint8_t count, uint16_t duration) {
  for (uint8_t i = 0; i < count; i++) {
    digitalWrite(LED_BUILTIN, HIGH);
    analogWrite(HAPTIC_PIN, 255);
    delay(duration / 2);
    digitalWrite(LED_BUILTIN, LOW);
    analogWrite(HAPTIC_PIN, 0);
    delay(duration / 2);
  }
}
